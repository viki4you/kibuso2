Rails.application.routes.draw do
  root  'welcome#index'
  get   'users/new'
  get   'users/login'

  resources :users
end
